/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : crm27

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 23/09/2021 17:51:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该商机属于哪个用户',
  `cust_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该商机来自于哪个客户',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '商机名称、标题',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '该商机的预估金额',
  `stage` int(11) NULL DEFAULT NULL COMMENT '该商机处于哪个阶段，必须是下列值之一：新挖掘 | 制定方案 | 谈判报价 | 结束',
  `end_status` tinyint(1) NULL DEFAULT NULL COMMENT '如果该商机的stage是结束阶段，本字段表示赢还是输',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_10`(`cust_id`) USING BTREE,
  INDEX `FK_Relationship_7`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_10` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_7` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10071 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '商机表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of business
-- ----------------------------
INSERT INTO `business` VALUES (1, 10003, 1, '江中', 10000000.00, 3, NULL);
INSERT INTO `business` VALUES (2, 10003, 2, 'dog', 20000000.00, 4, 1);
INSERT INTO `business` VALUES (3, 10003, 3, 'cat', 2000000.00, 4, 1);
INSERT INTO `business` VALUES (4, 10050, 11, '历史博物馆', 10000000.00, 2, NULL);
INSERT INTO `business` VALUES (5, 10004, 7, '推荐用户', 500000.00, 2, NULL);
INSERT INTO `business` VALUES (6, 10004, 8, '推荐用户', 300000.00, 4, 0);
INSERT INTO `business` VALUES (7, 10004, 9, '推荐用户', 1000000.00, 4, 1);
INSERT INTO `business` VALUES (8, 10013, 4, '短期金融工具推销', 30000000.00, 1, NULL);
INSERT INTO `business` VALUES (9, 10013, 5, '长期金融工具推销', 20000000.00, 2, NULL);
INSERT INTO `business` VALUES (10, 10013, 6, '汇票承兑', 50000000.00, 3, 1);
INSERT INTO `business` VALUES (11, 10045, 22, '销售光能量', 1000000.00, 4, 1);
INSERT INTO `business` VALUES (12, 10045, 23, '销售神光棒', 9999999.00, 4, 1);
INSERT INTO `business` VALUES (13, 10010, 19, '嘉华饼屋', 1000000.00, 2, NULL);
INSERT INTO `business` VALUES (14, 10010, 20, '过桥米线', 2000000.00, 3, NULL);
INSERT INTO `business` VALUES (15, 10010, 21, '金湖', 3000000.00, 4, 1);
INSERT INTO `business` VALUES (16, 10005, 11, '历史博物馆', 50000.00, 2, NULL);
INSERT INTO `business` VALUES (17, 10045, 24, '销售选票', 6666666.00, 4, 1);
INSERT INTO `business` VALUES (18, 10005, 14, '国家博物馆', 100000.00, 3, NULL);
INSERT INTO `business` VALUES (19, 10005, 15, '文化博物馆', 50000.00, 4, 0);
INSERT INTO `business` VALUES (20, 10012, 16, 'J-20订购', 90000000.00, 3, NULL);
INSERT INTO `business` VALUES (21, 10012, 17, 'FC-31订购', 80000000.00, 3, NULL);
INSERT INTO `business` VALUES (22, 10012, 18, 'Y-20订购', 90000000.00, 3, NULL);
INSERT INTO `business` VALUES (23, 10048, 30, '丸摩堂', 230000.00, 3, 0);
INSERT INTO `business` VALUES (24, 10048, 31, '蜜雪冰城', 400000.00, 1, 1);
INSERT INTO `business` VALUES (32, 10048, 29, '奈雪的茶', 1000000.00, 2, 1);
INSERT INTO `business` VALUES (10055, 10007, 29, '导弹', 1000000.00, 2, NULL);
INSERT INTO `business` VALUES (10056, 10007, 30, '坦克', 9000000.00, 3, 1);
INSERT INTO `business` VALUES (10057, 10007, 31, 'F-11', 10000000.00, 2, NULL);
INSERT INTO `business` VALUES (10063, 10000, 25, '税务局', 78989.00, 2, 0);
INSERT INTO `business` VALUES (10064, 10000, 25, '情报局', 999.00, 2, 0);
INSERT INTO `business` VALUES (10065, 10000, 25, '公安局', 110.00, 2, 0);
INSERT INTO `business` VALUES (10068, 10046, 37, '1000双运动鞋', 300000.00, 1, NULL);
INSERT INTO `business` VALUES (10069, 10046, 38, '1000杯奶茶', 9000.00, 4, 1);
INSERT INTO `business` VALUES (10070, 10046, 40, '500双女鞋', 50000.00, 4, 0);

-- ----------------------------
-- Table structure for business_trace
-- ----------------------------
DROP TABLE IF EXISTS `business_trace`;
CREATE TABLE `business_trace`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进是哪个user执行的',
  `business_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进跟进的是哪个商机',
  `contact_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进是跟客户公司哪个联系人联系的',
  `create_date` date NULL DEFAULT NULL COMMENT '本次跟进日期',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '跟进情况',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_18`(`contact_id`) USING BTREE,
  INDEX `FK_Relationship_8`(`user_id`) USING BTREE,
  INDEX `FK_Relationship_9`(`business_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_18` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_8` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_9` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '商机跟进记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of business_trace
-- ----------------------------
INSERT INTO `business_trace` VALUES (1, 10003, 1, 4, '2021-08-24', '商谈价格');
INSERT INTO `business_trace` VALUES (2, 10003, 2, 5, '2021-05-10', '签订合同');
INSERT INTO `business_trace` VALUES (3, 10003, 3, 6, '2021-08-24', '签订合同');
INSERT INTO `business_trace` VALUES (5, 10013, 8, 7, '2021-02-05', '讨价还价');
INSERT INTO `business_trace` VALUES (6, 10013, 9, 8, '2021-04-05', '磋商口水战');
INSERT INTO `business_trace` VALUES (7, 10013, 10, 9, '2021-06-05', '合同已拟定');
INSERT INTO `business_trace` VALUES (8, 10010, 13, 10, '2021-01-31', '糕点');
INSERT INTO `business_trace` VALUES (9, 10010, 14, 11, '2021-03-29', '米线');
INSERT INTO `business_trace` VALUES (10, 10010, 15, 15, '2021-07-09', '旅游');
INSERT INTO `business_trace` VALUES (11, 10004, 5, 1, '2020-02-20', '签订合同');
INSERT INTO `business_trace` VALUES (12, 10004, 6, 2, '2020-01-13', '金额的协商');
INSERT INTO `business_trace` VALUES (13, 10004, 7, 3, '2020-03-20', '签订合同');
INSERT INTO `business_trace` VALUES (16, 10045, 11, 27, '2021-08-24', '签订合同');
INSERT INTO `business_trace` VALUES (17, 10045, 12, 29, '2021-08-24', '签订合同');
INSERT INTO `business_trace` VALUES (18, 10045, 17, 32, '2021-08-24', '签订合同');
INSERT INTO `business_trace` VALUES (19, 10007, 10055, 18, '2011-06-06', '签合同');
INSERT INTO `business_trace` VALUES (20, 10007, 10056, 19, '2011-06-15', '签合同');
INSERT INTO `business_trace` VALUES (21, 10007, 10057, 20, '2011-07-13', '签合同');
INSERT INTO `business_trace` VALUES (22, 10005, 16, 12, '2021-04-24', '客户不满意');
INSERT INTO `business_trace` VALUES (23, 10005, 18, 13, '2021-04-25', '客户很生气');
INSERT INTO `business_trace` VALUES (24, 10005, 19, 14, '2021-04-26', '客户拿武器');
INSERT INTO `business_trace` VALUES (26, 10048, 23, 21, '2021-07-01', '合作愉快');
INSERT INTO `business_trace` VALUES (27, 10048, 24, 22, '2021-08-03', '不考虑');
INSERT INTO `business_trace` VALUES (28, 10048, 32, 23, '2021-08-27', '最后考虑中');
INSERT INTO `business_trace` VALUES (29, 10012, 20, 28, '2021-08-23', '即将签订合同');
INSERT INTO `business_trace` VALUES (30, 10012, 21, 30, '2021-08-23', '即将签订合同');
INSERT INTO `business_trace` VALUES (31, 10012, 22, 31, '2021-08-23', '即将签订合同');

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该联系人属于哪个客户',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系人姓名',
  `gender` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系人性别',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  `post` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '该联系人在客户公司里面的职务',
  `is_master` tinyint(1) NULL DEFAULT NULL COMMENT '表示该联系是不是决策人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_11`(`cust_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_11` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '联系人表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES (1, 7, '艾移动', '女', '13344445555', '总经理', 1);
INSERT INTO `contact` VALUES (2, 8, '习联通', '男', '14455556666', '总经理', 1);
INSERT INTO `contact` VALUES (3, 9, '李电信', '男', '15566667777', '总经理', 1);
INSERT INTO `contact` VALUES (4, 1, '何经理', '女', '13542197386', '总经理', 1);
INSERT INTO `contact` VALUES (5, 2, '史先生', '男', '17765437691', '总经理', 1);
INSERT INTO `contact` VALUES (6, 3, '何姐', '女', '17743152246', '业务员', 0);
INSERT INTO `contact` VALUES (7, 4, '欧浩辰', '男', '1433223', '财务部部长', 1);
INSERT INTO `contact` VALUES (8, 5, '池早早', '女', '258124841', '人事部部长', 0);
INSERT INTO `contact` VALUES (9, 6, '尔康', '男', '6565651516', '太子', 1);
INSERT INTO `contact` VALUES (10, 19, '旗旗子', '女', '19973666388', '总监', 1);
INSERT INTO `contact` VALUES (11, 20, '婷婷子', '女', '18736892457', '总经理', 1);
INSERT INTO `contact` VALUES (12, 11, '张经理', '女', '12345678901', '总经理', 1);
INSERT INTO `contact` VALUES (13, 14, '王先生', '男', '12345678931', '财务管理', 1);
INSERT INTO `contact` VALUES (14, 15, '李总', '女', '12345638901', '副总经理', 1);
INSERT INTO `contact` VALUES (15, 21, '沂沂子', '女', '18254742457', '部长', 1);
INSERT INTO `contact` VALUES (17, 29, '陈女士', '女', '1101018890', '教务部主管', 1);
INSERT INTO `contact` VALUES (18, 29, '美羊羊', '女', '18812345678', '采购部部长', 1);
INSERT INTO `contact` VALUES (19, 30, '红太狼', '女', '18823456789', '财务部部长', 1);
INSERT INTO `contact` VALUES (20, 31, '沸羊羊', '男', '18834567891', '战斗A队小队长', 0);
INSERT INTO `contact` VALUES (21, 29, '陈女士', '女', '1101018890', '教务部主管', 1);
INSERT INTO `contact` VALUES (22, 30, '刘先生', '男', '1425989374', '市场主管', 0);
INSERT INTO `contact` VALUES (23, 31, '夏先生', '男', '9876443314', '前台', 0);
INSERT INTO `contact` VALUES (24, 29, '陈女士', '女', '1101018890', '教务部主管', 1);
INSERT INTO `contact` VALUES (25, 30, '刘先生', '男', '1425989374', '市场主管', 0);
INSERT INTO `contact` VALUES (26, 31, '夏先生', '男', '9876443314', '前台', 0);
INSERT INTO `contact` VALUES (27, 22, '大古', '男', '17846375289', '业务员', 1);
INSERT INTO `contact` VALUES (28, 16, '奔波儿灞', '男', '18167891011', '大当家', 1);
INSERT INTO `contact` VALUES (29, 23, '盖亚', '男', '17384965289', '总经理', 1);
INSERT INTO `contact` VALUES (30, 17, '霸波尔奔', '男', '18167891012', '二当家', 1);
INSERT INTO `contact` VALUES (31, 18, '波奔儿爸', '男', '18167891013', '三当家', 1);
INSERT INTO `contact` VALUES (32, 24, '战忽局', '女', '17384969889', '业务员', 0);
INSERT INTO `contact` VALUES (33, 25, '张先生', '男', '7898989896', '局座', 1);
INSERT INTO `contact` VALUES (34, 25, '张先生', '男', '7898989896', '局座', 1);
INSERT INTO `contact` VALUES (35, 25, '毛先生', '男', '8888888888', '财务', 1);
INSERT INTO `contact` VALUES (36, 25, '习先生', '男', '99999999', '主席', 0);
INSERT INTO `contact` VALUES (37, 50, '陈全', '男', '15615231566', '跑腿一号', 1);
INSERT INTO `contact` VALUES (38, 51, '刘7', '男', '65187169462', '跑腿二号', 1);
INSERT INTO `contact` VALUES (39, 52, '张红', '女', '12658743254', '跑腿三号', 1);
INSERT INTO `contact` VALUES (40, 37, '张先生', '男', '1300000000', '经理', 1);
INSERT INTO `contact` VALUES (41, 38, '黄先生', '女', '1310000000', '采购部部长', 0);
INSERT INTO `contact` VALUES (42, 40, '代先生', '男', '1320000000', '财务部部长', 0);
INSERT INTO `contact` VALUES (44, 2, '江中', NULL, '5666', '老板', 1);
INSERT INTO `contact` VALUES (45, 2, '江中', NULL, '5666', '老板', 1);
INSERT INTO `contact` VALUES (46, 2, '江中', NULL, '5666', '老板', 1);
INSERT INTO `contact` VALUES (47, 2, '江中', NULL, '5666', '老板', 1);
INSERT INTO `contact` VALUES (48, 2, 'yyy', '男', '5666', '老板', 1);
INSERT INTO `contact` VALUES (49, 2, 'xxx', '女', 'dgdg', 'dgdg', 1);

-- ----------------------------
-- Table structure for contract
-- ----------------------------
DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该合同来自于哪个商机',
  `cust_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该合同来自哪个客户',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该合同属于哪个用户',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '合同的编号，如：XXYZ202108250043',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '合同名称、标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '合同内容',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '合同金额',
  `create_date` date NULL DEFAULT NULL COMMENT '该合同的签订日期/创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE,
  INDEX `FK_Relationship_13`(`cust_id`) USING BTREE,
  INDEX `FK_Relationship_14`(`user_id`) USING BTREE,
  INDEX `FK_Relationship_19`(`business_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_13` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_14` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_19` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '合同表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of contract
-- ----------------------------
INSERT INTO `contract` VALUES (1, 2, 2, 10003, '283-14151136', 'dog', '出售制造狗口粮的食品原料', 10000000.00, '2021-05-10');
INSERT INTO `contract` VALUES (2, 3, 3, 10003, '283-14151137', 'cat', '出售制造猫口粮的食品原料', 2000000.00, '2021-08-24');
INSERT INTO `contract` VALUES (3, 5, 7, 10004, 'XXYZ202108251045', '用户推荐定制方案', '给客户提供充足的用户资源', 500000.00, '2021-02-20');
INSERT INTO `contract` VALUES (4, 6, 8, 10004, 'XXYZ202108252065', '用户推荐定制方案', '给客户提供充足的用户资源', 800000.00, '2021-01-20');
INSERT INTO `contract` VALUES (5, 7, 9, 10004, 'XXYZ202108253078', '用户推荐定制方案', '给客户提供充足的用户资源', 1000000.00, '2021-03-20');
INSERT INTO `contract` VALUES (6, 20, 16, 10012, '202-2020202', 'J20订购', '向买家交付50架J20', 90000000.00, '2021-08-24');
INSERT INTO `contract` VALUES (7, 21, 17, 10012, '313-3131313', 'FC31订购', '向买家交付50架FC31', 80000000.00, '2021-08-24');
INSERT INTO `contract` VALUES (8, 22, 18, 10012, 'Y20-Y2Y2Y20', 'Y20订购', '向买家交付50架Y20', 90000000.00, '2021-08-24');
INSERT INTO `contract` VALUES (9, 16, 11, 10005, 'XXYZ38739874', '国家历史文化推广', '文化输出', 500000.00, '2021-05-23');
INSERT INTO `contract` VALUES (11, 18, 14, 10005, 'XXYZ38226874', '国家历史文化推广', '文化输出', 500000.00, '2021-05-23');
INSERT INTO `contract` VALUES (12, 19, 15, 10005, 'XXYZ56439874', '国家历史文化推广', '文化输出', 500000.00, '2021-05-23');
INSERT INTO `contract` VALUES (13, 8, 4, 10013, 'dqjr2021215', '短期金融工具出售协议', '给海丰银行提供短期金融工具的相关产品', 25000000.00, '2021-02-15');
INSERT INTO `contract` VALUES (14, 9, 5, 10013, 'cqjr2021415', '长期金融工具出售协议', '给河丰银行提供长期金融工具的相关产品', 55000000.00, '2021-04-15');
INSERT INTO `contract` VALUES (15, 10, 6, 10013, 'hp2021615', '汇票承兑相关协议', '给山丰银行提供汇票承兑的相关优惠措施', 30000000.00, '2021-06-15');
INSERT INTO `contract` VALUES (19, 10055, 29, 10007, 'abc12336743972', '导弹采购合同', '出售大量导弹', 1000000.00, '2011-09-18');
INSERT INTO `contract` VALUES (20, 10056, 30, 10007, 'abc456389784', '坦克采购合同', '出售大量坦克', 9000000.00, '2011-10-04');
INSERT INTO `contract` VALUES (21, 10057, 31, 10007, 'abc7893878947', '飞机采购合同', '出售大量飞机', 10000000.00, '2011-09-20');
INSERT INTO `contract` VALUES (25, 13, 19, 10010, 'XXYZ207638294755', '糕点加盟合同书', '糕点合同', 10000000.00, '2021-03-31');
INSERT INTO `contract` VALUES (26, 14, 20, 10010, 'XXYZ206254467857', '米线加盟合同书', '米线合同', 10000000.00, '2021-04-29');
INSERT INTO `contract` VALUES (27, 15, 21, 10010, 'XXYZ207364547474', '旅游合同书', '旅游合同', 20000000.00, '2021-08-09');
INSERT INTO `contract` VALUES (28, 11, 22, 10045, '999-2347698', '光能量销售合同', '销售光能量', 1000000.00, '2021-08-24');
INSERT INTO `contract` VALUES (29, 12, 23, 10045, '666-223598', '神光棒销售合同', '销售神光棒', 9999999.00, '2021-08-24');
INSERT INTO `contract` VALUES (30, 17, 24, 10045, '888-235346', '选票销售合同', '销售选票', 6666666.00, '2021-08-24');
INSERT INTO `contract` VALUES (31, 23, 26, 10048, 'qwe12438', '奶茶采购合同书', '慈善事业', 50000.00, '2021-07-01');
INSERT INTO `contract` VALUES (32, 24, 27, 10048, 'odsjs22', '奶茶采购合同书', '慈善事业', 50000.00, '2021-08-03');
INSERT INTO `contract` VALUES (33, 32, 28, 10048, 'sajgdiadi', '奶茶采购合同书', '慈善事业', 50000.00, '2021-08-27');

-- ----------------------------
-- Table structure for cust
-- ----------------------------
DROP TABLE IF EXISTS `cust`;
CREATE TABLE `cust`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该客户属于哪个user',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户名称',
  `from` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户来源，必须是下列值之一：广告，活动，线上咨询，口碑',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户公司所在地址',
  `create_date` date NULL DEFAULT NULL COMMENT '该客户的创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_4`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust
-- ----------------------------
INSERT INTO `cust` VALUES (1, 10003, '江中', '广告', '0281433287', '成都市二仙桥26号', '2020-11-03');
INSERT INTO `cust` VALUES (2, 10003, 'dog托儿所@', '广告', '0142354688', '深圳', '2019-12-29');
INSERT INTO `cust` VALUES (3, 10003, 'cat', '广告', '0142352358', '深圳', '2020-03-29');
INSERT INTO `cust` VALUES (4, 10013, '海丰银行', '广告', '6666666666', '海丰市杀马特区25号', '2020-02-15');
INSERT INTO `cust` VALUES (5, 10013, '河丰银行', '口碑', '65651692625', '河丰市老牛区5号', '2020-04-15');
INSERT INTO `cust` VALUES (6, 10013, '山丰银行', '活动', '16155565195', '山丰市嬢嬢区25号', '2020-06-15');
INSERT INTO `cust` VALUES (7, 10004, '移动网络', '活动', '111111111', '高新区888号一楼', '2020-11-11');
INSERT INTO `cust` VALUES (8, 10004, '联通网络', '活动', '222222222', '高新区888号二楼', '2020-11-11');
INSERT INTO `cust` VALUES (9, 10004, '电信网络', '活动', '333333333', '高新区888号三楼', '2020-11-11');
INSERT INTO `cust` VALUES (11, 10005, '历史博物馆', '活动', '13333300005', '四川省泸州市白招牌', '2021-01-22');
INSERT INTO `cust` VALUES (14, 10005, '国家博物馆', '广告', '13333300006', '四川省泸州市回龙湾', '2021-01-24');
INSERT INTO `cust` VALUES (15, 10005, '文化博物馆', '口碑', '13333300007', '四川省泸州市莲花池', '2021-01-11');
INSERT INTO `cust` VALUES (16, 10012, 'sasaleilei', '活动', '08731122325', '上海', '2019-02-05');
INSERT INTO `cust` VALUES (17, 10012, 'berleiberli', '口碑', '08785236974', '厦门', '2020-11-20');
INSERT INTO `cust` VALUES (18, 10012, 'yohoyoho', '活动', '087354852156', '台北', '2020-08-05');
INSERT INTO `cust` VALUES (19, 10010, '嘉华饼屋', '口碑', '18899666666', '云南省昆明市西山区', '2021-01-30');
INSERT INTO `cust` VALUES (20, 10010, '过桥米线', '线上咨询', '16724675555', '云南省红河州蒙自市', '2021-03-28');
INSERT INTO `cust` VALUES (21, 10010, '金湖', '活动', '16753820555', '云南省红河州个旧市', '2021-07-08');
INSERT INTO `cust` VALUES (22, 10045, '赛文', '广告', '17812342345', 'M78星云', '2021-08-24');
INSERT INTO `cust` VALUES (23, 10045, '迪迦', '口碑', '17812327845', 'M78星云', '2021-08-24');
INSERT INTO `cust` VALUES (24, 10045, '懂王', '线上咨询', '17812322735', '美利坚', '2021-08-24');
INSERT INTO `cust` VALUES (25, 10007, 'xiyangyang', '活动', '13312345678', '青青草原羊村', '2011-09-17');
INSERT INTO `cust` VALUES (26, 10048, '丸摩堂', '口碑', '1101927363538', '四川省成都市武侯区', '2021-06-20');
INSERT INTO `cust` VALUES (27, 10048, '奈雪的茶', '活动', '-78512078', '广东省广州市白云区', '2021-07-11');
INSERT INTO `cust` VALUES (28, 10048, '蜜雪冰城', '广告', '-19373147', '河南省郑州市', '2021-08-01');
INSERT INTO `cust` VALUES (29, 10007, '喜羊羊', '活动', '13312345678', '青青草原羊村', '2011-09-17');
INSERT INTO `cust` VALUES (30, 10007, '灰太狼', '广告', '1332356781', '青青草原狼堡', '2011-10-03');
INSERT INTO `cust` VALUES (31, 10007, '懒羊羊', '活动', '133345678912', '青青草原羊村', '2011-09-19');
INSERT INTO `cust` VALUES (37, 10046, '鸿星尔克', '线上咨询', '12000000000', '北京', '2021-08-24');
INSERT INTO `cust` VALUES (38, 10046, '蜜雪冰城', '活动', '12200000000', '成都', '2021-07-20');
INSERT INTO `cust` VALUES (40, 10046, '李宁', '广告', '12100000000', '上海', '2020-04-20');
INSERT INTO `cust` VALUES (41, 10000, '蓝精灵', '广告', '76565656', '成都市天府新谷', '1988-01-01');
INSERT INTO `cust` VALUES (42, 10000, '滴露', '广告', '76565656', '成都市天府新谷', '2000-01-01');
INSERT INTO `cust` VALUES (43, 10000, '大王', '广告', '76565656', '成都市天府新谷', '2001-01-01');
INSERT INTO `cust` VALUES (50, 10049, '全家', '广告', '15327642545', '四川省成都市高新区天府五街店', '2021-07-21');
INSERT INTO `cust` VALUES (51, 10049, '711', '活动', '156322652651', '四川省成都市高新区天府四街店', '2021-06-21');
INSERT INTO `cust` VALUES (52, 10049, '红旗', '口碑', '131623161616', '四川省成都市高新区天府三街店', '2021-05-21');
INSERT INTO `cust` VALUES (53, 10046, '鸿星尔克', '线上咨询', '12000000000', '北京', '2021-06-26');
INSERT INTO `cust` VALUES (54, 10046, '鸿星尔克', '线上咨询', '12000000000', '北京', '2021-06-26');
INSERT INTO `cust` VALUES (55, 10046, '李宁', '广告', '12100000000', '上海', '2020-02-22');
INSERT INTO `cust` VALUES (56, 10046, '蜜雪冰城', '活动', '12200000000', '成都', '2021-02-20');
INSERT INTO `cust` VALUES (57, 1, '用于测试', '活动', '666', '不知道', '2021-09-03');
INSERT INTO `cust` VALUES (58, 1, 'xxx', '广告', 'xxx', 'xxxxxx', '2021-09-14');
INSERT INTO `cust` VALUES (59, 1, 'ccc', '广告', 'cccc', 'cccccc', '2021-09-14');
INSERT INTO `cust` VALUES (60, 1, '新的线索C', '广告', '55555555', '不晓得', '2021-09-15');
INSERT INTO `cust` VALUES (61, 1, '新的线索CC', '广告', '', '', '2021-09-15');
INSERT INTO `cust` VALUES (62, 10003, '云南白药', '口碑', '0281443366', '云南省曲靖市马龙县233号', '2021-09-16');
INSERT INTO `cust` VALUES (63, 1, NULL, NULL, NULL, NULL, '2021-09-23');
INSERT INTO `cust` VALUES (64, 1, '你好呀', NULL, '666666', NULL, '2021-09-23');
INSERT INTO `cust` VALUES (65, 1, 'xxx', NULL, '666666666', NULL, '2021-09-23');

-- ----------------------------
-- Table structure for cust_trace
-- ----------------------------
DROP TABLE IF EXISTS `cust_trace`;
CREATE TABLE `cust_trace`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进是对哪个客户的跟进',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进是谁执行的',
  `contact_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示本次跟进是跟客户公司里面哪个联系人联系的',
  `create_date` date NULL DEFAULT NULL COMMENT '本次跟进日期',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '跟进情况',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_17`(`contact_id`) USING BTREE,
  INDEX `FK_Relationship_5`(`cust_id`) USING BTREE,
  INDEX `FK_Relationship_6`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_17` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_5` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户跟进记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_trace
-- ----------------------------
INSERT INTO `cust_trace` VALUES (1, 1, 10003, 4, '2021-06-12', '希望合作能更进一步');
INSERT INTO `cust_trace` VALUES (2, 2, 10003, 5, '2021-05-09', '约定时间签订合同');
INSERT INTO `cust_trace` VALUES (3, 3, 10003, 6, '2021-08-20', '约定时间签订合同');
INSERT INTO `cust_trace` VALUES (4, 4, 10013, 7, '2021-01-25', '有意向购买我司的产品');
INSERT INTO `cust_trace` VALUES (5, 5, 10013, 8, '2021-03-25', '特别想购买我司的产品');
INSERT INTO `cust_trace` VALUES (6, 6, 10013, 9, '2021-05-25', '差点直接签合同');
INSERT INTO `cust_trace` VALUES (7, 19, 10010, 10, '2021-01-31', '2021年1月30日成为客户');
INSERT INTO `cust_trace` VALUES (8, 20, 10010, 11, '2021-03-29', '2021年3月28日成为客户');
INSERT INTO `cust_trace` VALUES (9, 21, 10010, 15, '2021-07-09', '2021年7月8日成为客户');
INSERT INTO `cust_trace` VALUES (10, 7, 10004, 1, '2020-02-15', '方案商定完毕，准备签订合同');
INSERT INTO `cust_trace` VALUES (11, 8, 10004, 2, '2020-01-12', '对合同方案满意，正在对金额进行商定');
INSERT INTO `cust_trace` VALUES (12, 9, 10004, 3, '2020-03-16', '方案商定完毕，准备签订合同');
INSERT INTO `cust_trace` VALUES (13, 11, 10005, 12, '2021-04-06', '客户已和其他公司签约');
INSERT INTO `cust_trace` VALUES (14, 14, 10005, 13, '2021-05-06', '谈判破裂');
INSERT INTO `cust_trace` VALUES (15, 15, 10005, 14, '2021-06-06', '客户之间竞争激烈');
INSERT INTO `cust_trace` VALUES (20, 29, 10007, 18, '2012-03-07', '可以进一步提供各种武器');
INSERT INTO `cust_trace` VALUES (21, 30, 10007, 19, '2012-03-07', '可以进一步提供各种武器');
INSERT INTO `cust_trace` VALUES (22, 31, 10007, 20, '2012-03-07', '可以进一步提供各种武器');
INSERT INTO `cust_trace` VALUES (23, 26, 10048, 29, '2021-06-30', '考虑中');
INSERT INTO `cust_trace` VALUES (24, 27, 10048, 30, '2021-08-01', '还有问题需要商议');
INSERT INTO `cust_trace` VALUES (25, 28, 10048, 31, '2021-08-24', '可以进一步考虑');
INSERT INTO `cust_trace` VALUES (26, 16, 10012, 28, '2021-08-22', '双方就应该尽快签订合同方面达成共识');
INSERT INTO `cust_trace` VALUES (27, 17, 10012, 30, '2021-08-22', '双方就应该尽快签订合同方面达成共识');
INSERT INTO `cust_trace` VALUES (28, 18, 10012, 31, '2021-08-22', '双方就应该尽快签订合同方面达成共识');
INSERT INTO `cust_trace` VALUES (29, 22, 10045, 27, '2021-08-24', '马上签订合同');
INSERT INTO `cust_trace` VALUES (30, 23, 10045, 29, '2021-08-24', '马上签订合同');
INSERT INTO `cust_trace` VALUES (31, 24, 10045, 32, '2021-08-24', '马上签订合同');
INSERT INTO `cust_trace` VALUES (32, 50, 10049, 37, '2021-07-22', '沟通愉快');
INSERT INTO `cust_trace` VALUES (33, 51, 10049, 38, '2021-06-22', '沟通愉快');
INSERT INTO `cust_trace` VALUES (34, 52, 10049, 39, '2021-05-22', '沟通愉快');

-- ----------------------------
-- Table structure for lead
-- ----------------------------
DROP TABLE IF EXISTS `lead`;
CREATE TABLE `lead`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该线索属于哪个user',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '线索名称',
  `from` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '线索来源，必须是下列值之一：广告，活动，线上咨询，口碑',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `create_date` date NULL DEFAULT NULL COMMENT '该线索的创建日期（入库日期）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_1`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '线索表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lead
-- ----------------------------
INSERT INTO `lead` VALUES (4, 10003, '白鹭制药厂', '广告', '0281433286', '四川省成都市都江堰市白鹭大道28号', '2021-07-12');
INSERT INTO `lead` VALUES (7, 10010, '嘉华饼屋', '口碑', '18899666666', '云南省昆明市西山区', '2020-12-28');
INSERT INTO `lead` VALUES (8, 10004, '京东物流', '口碑', '082135654', '武侯区天府大道666号', '2020-05-18');
INSERT INTO `lead` VALUES (9, 10013, '海丰银行', '广告', '6666666666', '海丰市杀马特区25号', '2020-01-05');
INSERT INTO `lead` VALUES (10, 10013, '河丰银行', '口碑', '65651692625', '河丰市老牛区5号', '2020-03-05');
INSERT INTO `lead` VALUES (11, 10013, '山丰银行', '活动', '16155565195', '山丰市嬢嬢区25号', '2020-05-05');
INSERT INTO `lead` VALUES (13, 10004, '顺丰物流', '口碑', '983256782', '武侯区天府大道777号', '2020-06-18');
INSERT INTO `lead` VALUES (14, 10004, '每日通讯', '广告', '213532123', '武侯区天府大道888号', '2019-12-18');
INSERT INTO `lead` VALUES (15, 10005, '宝光药业', '广告', '134555559', '四川省泸州市江阳区', '2021-07-04');
INSERT INTO `lead` VALUES (16, 10007, '微软', '口碑', '18988888888', '美国华盛顿州Redmond市 NE39号街道', '2019-07-09');
INSERT INTO `lead` VALUES (17, 10007, '中国农业银行', '广告', '18966666666', '四川省成都市郫都区红光街道高店路西段67号', '2012-06-01');
INSERT INTO `lead` VALUES (18, 10007, '中国电信', '口碑', '18911111111', '四川省成都市武侯区世纪城路1049号', '2010-02-23');
INSERT INTO `lead` VALUES (19, 10010, '过桥米线', '线上咨询', '16724675555', '云南省红河州蒙自市', '2021-01-07');
INSERT INTO `lead` VALUES (20, 10005, '绿叶制药', '口碑', '134555568', '四川省泸州市龙马潭区', '2021-06-07');
INSERT INTO `lead` VALUES (21, 10010, '金湖', '活动', '16753820555', '云南省红河州个旧市', '2021-05-18');
INSERT INTO `lead` VALUES (22, 10005, '阳光医疗器械', '广告', '134555559', '四川省泸州市金山路', '2021-05-09');
INSERT INTO `lead` VALUES (32, 10048, '一点点', '广告', '-88806776', '上海市普陀区真南路822弄129支弄1号楼309室', '2021-04-11');
INSERT INTO `lead` VALUES (36, 10048, '茶百道', '活动', '2708', '成都市温江区光华大道三段1588号珠江国际写字楼20楼02-06号', '2021-05-10');
INSERT INTO `lead` VALUES (37, 10048, '喜茶', '广告', '-83515238', '广东省广州市越秀区惠福东路610号', '2021-06-19');
INSERT INTO `lead` VALUES (41, 10012, '沈飞', '口碑', '08731616044', '沈阳', '2021-11-02');
INSERT INTO `lead` VALUES (42, 10012, '西飞', '口碑', '08731616055', '西安', '2021-11-03');
INSERT INTO `lead` VALUES (43, 10012, '成飞', '口碑', '08731616066', '成都', '2021-11-04');
INSERT INTO `lead` VALUES (44, 10045, '瞌睡乔', '广告', '18467272746', '美利坚', '2021-08-24');
INSERT INTO `lead` VALUES (45, 10045, '雷欧', '口碑', '18467224786', 'M78星云', '2021-08-24');
INSERT INTO `lead` VALUES (46, 10045, '梦比优斯', '口碑', '18234224776', 'M78星云', '2021-08-24');
INSERT INTO `lead` VALUES (50, 10049, '全家', '广告', '15327642545', '四川省成都市高新区天府五街店', '2021-07-21');
INSERT INTO `lead` VALUES (51, 10049, '711', '活动', '156322652651', '四川省成都市高新区天府四街店', '2021-06-21');
INSERT INTO `lead` VALUES (52, 10049, '红旗', '口碑', '131623161616', '四川省成都市高新区天府三街店', '2021-05-21');
INSERT INTO `lead` VALUES (53, 10000, '蓝月亮', '广告', '987654321', '成都市天府新谷', '2020-01-01');
INSERT INTO `lead` VALUES (54, 10000, '蓝月亮', '广告', '987654321', '成都市天府新谷', '2020-01-01');
INSERT INTO `lead` VALUES (55, 10000, '六神', '广告', '76565656', '成都市天府新谷', '1998-01-01');
INSERT INTO `lead` VALUES (56, 10000, '花王', '广告', '97878321', '成都市天府新谷', '2000-01-09');
INSERT INTO `lead` VALUES (57, 10000, '滴露', '广告', '76565656', '成都市天府新谷', '2000-01-01');
INSERT INTO `lead` VALUES (58, 10000, '大王', '广告', '76565656', '成都市天府新谷', '2001-01-01');
INSERT INTO `lead` VALUES (59, 10000, '滴露', '广告', '76565656', '成都市天府新谷', '2000-01-01');
INSERT INTO `lead` VALUES (60, 10000, '大王', '广告', '76565656', '成都市天府新谷', '2001-01-01');
INSERT INTO `lead` VALUES (61, 10000, '滴露', '广告', '76565656', '成都市天府新谷', '2000-01-01');
INSERT INTO `lead` VALUES (62, 10000, '大王', '广告', '76565656', '成都市天府新谷', '2001-01-01');
INSERT INTO `lead` VALUES (63, 10046, '鸿星尔克', '线上咨询', '12000000000', '北京', '2021-06-26');
INSERT INTO `lead` VALUES (64, 10046, '鸿星尔克', '线上咨询', '12000000000', '北京', '2021-06-26');
INSERT INTO `lead` VALUES (65, 10046, '李宁', '广告', '12100000000', '上海', '2020-02-22');
INSERT INTO `lead` VALUES (66, 10046, '蜜雪冰城', '活动', '12200000000', '成都', '2021-02-20');
INSERT INTO `lead` VALUES (67, 1, '新的线索C', '广告', '55555555', '不晓得', '2021-09-14');
INSERT INTO `lead` VALUES (68, 1, '新的线索C', '广告', '55555555', '不晓得', '2021-09-14');
INSERT INTO `lead` VALUES (71, 1, '白鹭制药厂', '广告', '0281433286', '四川省成都市都江堰市白鹭大道28号', '2021-09-14');
INSERT INTO `lead` VALUES (72, 1, '白鹭制药厂', '广告', '0281433286', '四川省成都市都江堰市白鹭大道28号', '2021-09-14');
INSERT INTO `lead` VALUES (74, 1, '云南白药cc', '口碑', '0281443366', '云南省曲靖市马龙县233号', '2021-09-14');
INSERT INTO `lead` VALUES (75, 1, 'SpringBootTest', NULL, NULL, NULL, '2021-09-23');

-- ----------------------------
-- Table structure for lead_trace
-- ----------------------------
DROP TABLE IF EXISTS `lead_trace`;
CREATE TABLE `lead_trace`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示执行跟进的user',
  `lead_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示跟进的是哪条线索',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '跟进情况，跟进内容',
  `create_date` date NULL DEFAULT NULL COMMENT '跟进日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_2`(`user_id`) USING BTREE,
  INDEX `FK_Relationship_3`(`lead_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_3` FOREIGN KEY (`lead_id`) REFERENCES `lead` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '线索跟进记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lead_trace
-- ----------------------------
INSERT INTO `lead_trace` VALUES (1, 10010, 7, '2020年12月28日成为线索', '2020-12-29');
INSERT INTO `lead_trace` VALUES (2, 10010, 19, '2021年1月7日成为线索', '2021-01-08');
INSERT INTO `lead_trace` VALUES (3, 10010, 21, '2021年5月18日成为线索', '2021-05-19');
INSERT INTO `lead_trace` VALUES (7, 10003, 4, '2021年7月12日成为线索', '2021-07-12');
INSERT INTO `lead_trace` VALUES (8, 10013, 9, '电话邀约，线下面谈', '2021-01-15');
INSERT INTO `lead_trace` VALUES (9, 10013, 10, '座机邀约，腾讯会议交谈', '2021-03-15');
INSERT INTO `lead_trace` VALUES (10, 10013, 11, 'QQ电话，微信协商', '2021-05-15');
INSERT INTO `lead_trace` VALUES (11, 10004, 8, '微信线上推荐方案', '2020-05-25');
INSERT INTO `lead_trace` VALUES (12, 10004, 13, '微信线上推荐方案', '2020-06-22');
INSERT INTO `lead_trace` VALUES (13, 10004, 14, '微信线上推荐方案', '2019-12-20');
INSERT INTO `lead_trace` VALUES (14, 10045, 44, '推荐脑白金', '2021-08-24');
INSERT INTO `lead_trace` VALUES (15, 10045, 45, '介绍新式宇宙飞船', '2021-08-24');
INSERT INTO `lead_trace` VALUES (16, 10045, 46, '推荐安全保险', '2021-08-24');
INSERT INTO `lead_trace` VALUES (19, 10012, 41, '前线急需支援', '2021-08-23');
INSERT INTO `lead_trace` VALUES (20, 10012, 42, '前线急需支援', '2021-08-23');
INSERT INTO `lead_trace` VALUES (21, 10012, 43, '前线急需支援', '2021-08-23');
INSERT INTO `lead_trace` VALUES (24, 10007, 16, '准备吃顿饭', '2019-07-10');
INSERT INTO `lead_trace` VALUES (25, 10007, 17, '已经加qq了', '2012-06-07');
INSERT INTO `lead_trace` VALUES (26, 10007, 18, '快要卖出去了', '2010-03-18');
INSERT INTO `lead_trace` VALUES (28, 10005, 15, '激烈探讨生发剂的效果', '2021-04-24');
INSERT INTO `lead_trace` VALUES (29, 10048, 32, '邀请到公司参观', '2021-05-28');
INSERT INTO `lead_trace` VALUES (31, 10005, 20, '探讨长生药的效果', '2021-04-25');
INSERT INTO `lead_trace` VALUES (32, 10005, 22, '探讨毒鼠强的效果', '2021-04-14');
INSERT INTO `lead_trace` VALUES (33, 10048, 36, '腾讯会议商议', '2021-07-30');
INSERT INTO `lead_trace` VALUES (34, 10048, 37, '打电话沟通', '2021-08-23');
INSERT INTO `lead_trace` VALUES (35, 10049, 50, '建议引进进口奶粉', '2021-07-20');
INSERT INTO `lead_trace` VALUES (36, 10049, 51, '建议引进进口尿不湿', '2021-06-20');
INSERT INTO `lead_trace` VALUES (37, 10049, 52, '建议引进进口奶嘴', '2021-05-20');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该笔回款是哪个合同的待收款',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '外键，表示该笔回款是哪个用户去收的',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '回款金额',
  `create_date` date NULL DEFAULT NULL COMMENT '回款日期',
  `pay_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '支付方式，必须是下列值之一：现金，支票，银行转账，支付宝，微信',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Relationship_15`(`contract_id`) USING BTREE,
  INDEX `FK_Relationship_16`(`user_id`) USING BTREE,
  CONSTRAINT `FK_Relationship_15` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Relationship_16` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '回款表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES (1, 3, 10004, 500000.00, '2021-04-01', '银行转账');
INSERT INTO `payment` VALUES (2, 4, 10004, 800000.00, '2021-03-01', '银行转账');
INSERT INTO `payment` VALUES (3, 5, 10004, 1000000.00, '2021-05-01', '银行转账');
INSERT INTO `payment` VALUES (4, 1, 10003, 400000.00, '2021-06-10', '银行转账');
INSERT INTO `payment` VALUES (5, 1, 10003, 400000.00, '2021-07-10', '银行转账');
INSERT INTO `payment` VALUES (6, 1, 10003, 400000.00, '2021-08-10', '银行转账');
INSERT INTO `payment` VALUES (7, 13, 10013, 25000000.00, '2021-03-01', '支票');
INSERT INTO `payment` VALUES (8, 14, 10013, 55000000.00, '2021-05-01', '支付宝');
INSERT INTO `payment` VALUES (9, 15, 10013, 30000000.00, '2021-07-01', '微信');
INSERT INTO `payment` VALUES (13, 19, 10007, 1000000.00, '2011-07-01', '银行转账');
INSERT INTO `payment` VALUES (14, 20, 10007, 9000000.00, '2011-07-01', '支付宝');
INSERT INTO `payment` VALUES (15, 21, 10007, 10000000.00, '2011-07-01', '现金');
INSERT INTO `payment` VALUES (16, 25, 10010, 50000000.00, '2021-04-30', '银行转账');
INSERT INTO `payment` VALUES (17, 26, 10010, 10000000.00, '2021-05-29', '银行转账');
INSERT INTO `payment` VALUES (18, 27, 10010, 1500000.00, '2021-10-01', '银行转账');
INSERT INTO `payment` VALUES (19, 9, 10005, 500000.00, '2021-07-12', '微信');
INSERT INTO `payment` VALUES (20, 11, 10005, 500000.00, '2021-07-13', '支付宝');
INSERT INTO `payment` VALUES (21, 12, 10005, 500000.00, '2021-07-14', '支票');
INSERT INTO `payment` VALUES (22, 28, 10045, 1000000.00, '2021-08-24', '银行转账');
INSERT INTO `payment` VALUES (23, 29, 10045, 9999999.00, '2021-08-24', '银行转账');
INSERT INTO `payment` VALUES (24, 30, 10045, 6666666.00, '2021-08-24', '银行转账');
INSERT INTO `payment` VALUES (25, 31, 10048, 50000.00, '2021-08-28', '现金');
INSERT INTO `payment` VALUES (26, 32, 10048, 50000.00, '2021-08-28', '现金');
INSERT INTO `payment` VALUES (27, 33, 10048, 50000.00, '2021-08-28', '现金');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户名，用来登录的，应该是唯一的',
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '密码的密文',
  `realname` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户的真实姓名',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10058 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户表（用户指的是使用CRM系统的公司的员工）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'zima', '666666', '芝麻', 'zima@qq.com', '13333333333');
INSERT INTO `user` VALUES (10000, 'keke', '666666', '徐丽佳', 'keke@qq.com', '66666666');
INSERT INTO `user` VALUES (10003, 'lyj', '666666', '林仡杰', '2754074558@qq.com', '17628077216');
INSERT INTO `user` VALUES (10004, 'tom', '666666', '汤姆', 'tom@qq.com', '16788875432');
INSERT INTO `user` VALUES (10005, 'zhangziwei', '666666', '张紫薇', 'zzw@qq.com', '11100022233');
INSERT INTO `user` VALUES (10007, 'hmbb', '666666', '海绵宝宝', 'hmbb@qq.com', '13666666666');
INSERT INTO `user` VALUES (10010, 'LIKEYI', '666666', '李柯沂', 'likeyi@qq.com', '13377689001');
INSERT INTO `user` VALUES (10012, 'wzm', '666666', '王子沫', 'wzm@163.com', '15167891011');
INSERT INTO `user` VALUES (10013, '派大星', '666666', '陈豪', '2476664917@qq.com', '8208208820');
INSERT INTO `user` VALUES (10045, 'yy', '666666', '严尹', '3235473242@qq.com', '13264745672');
INSERT INTO `user` VALUES (10046, 'beauty_h', '666666', '黄筱', 'beauty_h@qq.com', '13666666666');
INSERT INTO `user` VALUES (10048, '藤井', '666666', '陈敏', 'fujii@qq.com', '12345678901');
INSERT INTO `user` VALUES (10049, 'zon', '666666', '唐宗帅', 'von@qq.com', '13158600807');
INSERT INTO `user` VALUES (10050, '卡梅尔想睡觉', '666666', '莫黛竹', '2692974969@qq.com', '19981471152');
INSERT INTO `user` VALUES (10054, 'Camille', '666666', '莫黛竹', '2692974969@qq.com', '19981471152');
INSERT INTO `user` VALUES (10057, 'Lucy', '666666', NULL, NULL, 'None');

SET FOREIGN_KEY_CHECKS = 1;
