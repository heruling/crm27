import Vue from "vue";
import "./plugins/axios";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import App from "./App.vue";
import router from "./router"; // 加载路由器对象
import store from "./store";

Vue.use(ElementUI);
Vue.config.productionTip = false;

new Vue({
    router, // 把路由器对象注入到Root组件
    store, // 把全局仓储对象store注入到Root组件
    render: h => h(App)
}).$mount("#app");
