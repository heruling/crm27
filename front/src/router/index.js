import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";

Vue.use(VueRouter);

// 定义路由表
const routes = [
    {
        path: "/",
        name: "Login",
        component: Login
    },
    {
        path: "/home",
        name: "Home",
        component: () => import("../views/Home.vue"),
        children: [
            {
                path: "/dashboard",
                name: "Dashboard",
                component: () => import("../views/dashboard/Index.vue")
            },
            {
                path: "/lead",
                name: "Lead",
                component: () => import("../views/lead/Index.vue")
            },
            {
                path: "/cust",
                name: "Cust",
                component: () => import("../views/cust/Index.vue")
            },
            {
                path: "/contact",
                name: "Contact",
                component: () => import("../views/contact/Index.vue")
            },
            {
                path: "/business",
                name: "Business",
                component: () => import("../views/business/Index.vue")
            },
            {
                path: "/contract",
                name: "Contract",
                component: () => import("../views/contract/Index.vue")
            },
            {
                path: "/payment",
                name: "Payment",
                component: () => import("../views/payment/Index.vue")
            }
        ]
    }
];

// 创建路由器对象
const router = new VueRouter({
    routes
});

export default router;
