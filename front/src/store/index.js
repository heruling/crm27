// Vuex 专业store模式

import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

// 负责对state进行持久化
const DB = {
    persistUser(user) {
        sessionStorage.setItem("user", JSON.stringify(user));
    },
    loadUser() {
        const jsonString = sessionStorage.getItem("user");
        if (jsonString) {
            const user = JSON.parse(jsonString);
            return user;
        }
        return {};
    }
};

// 仓储对象store，它必须是Vuex.Store类型的实例
const store = new Vuex.Store({
    // 全局状态
    state: {
        user: DB.loadUser()
    },
    // 变更状态的方法
    mutations: {
        setUser(state, authentication) {
            state.user = authentication;
            // 持久化存储到浏览器的sessionStorage
            DB.persistUser(authentication);
        }
    }
});

export default store;
