// 导入axios库
import axios from "axios";
import Vue from "vue";
import store from "../store";

// 全局axios配置
const _axios = axios.create({
    baseURL: "http://localhost:8080"
});

// 添加请求拦截器
_axios.interceptors.request.use(
    function(config) {
        // 在发送请求之前做些什么: 获取store.state.user.id，然后把这个当前用户id带在请求头中发送给后端API
        const userId = store.state.user.id;
        if (userId) {
            config.headers["Auth-Token"] = userId;
        }
        return config;
    },
    function(error) {
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

// 添加响应拦截器
_axios.interceptors.response.use(
    function(response) {
        // 对响应数据做点什么
        return response.data;
    },
    function(error) {
        // 对响应错误做点什么
        return Promise.reject(error);
    }
);

// 定义一个Vue插件，把axios当作Vue框架的一个插件来使用
const AxiosPlugin = {
    install(Vue) {
        // 添加全局属性
        Vue.axios = _axios;
        // 添加实例属性
        Vue.prototype.axios = _axios;
        Vue.prototype.$axios = _axios;
        Vue.prototype.$http = _axios;
    }
};

// 利用Vue的插件机制（use）把_axios注册为Vue实例的属性
// 这样你在你的.vue文件中就可以用this.axios / this.$axios / this.$http来调用axios API了
Vue.use(AxiosPlugin);

export default AxiosPlugin;
