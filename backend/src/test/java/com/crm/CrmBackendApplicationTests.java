package com.crm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.crm.service.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootTest
@Transactional // 执行完操作后自动回滚，这样不会让单元测试污染数据库
class CrmBackendApplicationTests {
	
	@Autowired
	LeadService leadService;
	
	@Test
	void contextLoads() throws JsonProcessingException {
	}

}
