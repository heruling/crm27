package com.crm.service.impl;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.crm.entity.Contact;
import com.crm.service.ContactService;
import com.crm.vo.SearchResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@Transactional
class ContactServiceImplTest {
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testSave() {
		fail("Not yet implemented");
	}

	@Test
	void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	void testFindById() throws JsonProcessingException {
		Contact contact = this.contactService.findById(1);
		System.out.println(objectMapper.writeValueAsString(contact));
	}

	@Test
	void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	void testSearch() throws JsonProcessingException {
		SearchResult<Contact> searchResult = this.contactService.search("", 5, 2);
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(searchResult));
	}

}
