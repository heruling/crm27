package com.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Lead;
import com.crm.vo.ApiResult;

@CrossOrigin
@RestController
public class LeadController {
	
	@PutMapping("/leads")
	public ApiResult update(@RequestBody Lead lead) {
		return null;
		
	}
	
	@PutMapping("/leads/{id}")
	public ApiResult changeToBeCust(@PathVariable int id) {
		return null;
		
	}
}
