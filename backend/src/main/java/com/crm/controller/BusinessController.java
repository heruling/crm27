package com.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Business;
import com.crm.service.BusinessService;

@CrossOrigin
@RestController
@RequestMapping("/businesses")
public class BusinessController extends BaseController<Business, BusinessService> {

}
