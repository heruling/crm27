package com.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.crm.service.BaseService;
import com.crm.vo.ApiResult;
import com.crm.vo.SearchResult;

@CrossOrigin
public abstract class BaseController<T, TService extends BaseService<T>> {
	@Autowired
	private TService service;
	
	@GetMapping
	public ApiResult search(String q, int size, int page) {
		SearchResult<T> searchResult = this.service.search(q, size, page);
		return ApiResult.ok(searchResult);
	}
	
	@PostMapping
	public ApiResult save(@RequestBody T t) {
		this.service.save(t);
		return ApiResult.ok();
	}
	
	@GetMapping("/{id}")
	public ApiResult findById(@PathVariable int id) {
		T t = this.service.findById(id);
		return ApiResult.ok(t);
	}
	
	@PutMapping
	public ApiResult update(@RequestBody T t) {
		this.service.update(t);
		return ApiResult.ok();
	}
	
	@DeleteMapping("/{id}")
	public ApiResult delete(@PathVariable int id) {
		this.service.delete(id);
		return ApiResult.ok();
	}
}
