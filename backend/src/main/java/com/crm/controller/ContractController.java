package com.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Contract;
import com.crm.service.ContractService;

@CrossOrigin
@RestController
@RequestMapping("/contracts")
public class ContractController extends BaseController<Contract, ContractService> {
	
}
