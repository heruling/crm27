package com.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Payment;
import com.crm.service.PaymentService;

@CrossOrigin
@RestController
@RequestMapping("/payments")
public class PaymentController extends BaseController<Payment, PaymentService> {

}
