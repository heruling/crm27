package com.crm.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.User;
import com.crm.service.UserService;
import com.crm.vo.ApiResult;

@CrossOrigin
@RestController
public class UserController {
	
	public UserController() {
		System.out.println("========================= spring framework 在 new UserController ================================");
	}
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/login")
	public ApiResult login(String username, String password) {
		User user = this.userService.authenticate(username, password);
		if (Objects.isNull(user)) {
			return ApiResult.error(ApiResult.NOT_AUTHENTICATED, "用户名或密码错误");
		}
		return ApiResult.ok(user);
	}

}
