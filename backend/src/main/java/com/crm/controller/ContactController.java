package com.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Contact;
import com.crm.service.ContactService;

@CrossOrigin
@RestController
@RequestMapping("/contacts")
public class ContactController extends BaseController<Contact, ContactService> {
	
	
}
