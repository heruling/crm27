package com.crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.entity.Cust;
import com.crm.service.CustService;
import com.crm.vo.ApiResult;
import com.crm.vo.SearchResult;

@CrossOrigin
@RestController
public class CustController {

	@Autowired
	private CustService custService;
	
	
	// handler methods
	
	@GetMapping("/custs/search-by-name")
	public ApiResult searchByName(String query) {
		List<Cust> list = this.custService.searchByName(query);
		return ApiResult.ok(list);
	}
	
	@GetMapping("/custs")
	public ApiResult search(String q, int size, int page) {
		SearchResult<Cust> searchResult = this.custService.search(q, size, page);
		return ApiResult.ok(searchResult);
	}
	
	@PostMapping("/custs")
	public ApiResult save(@RequestBody Cust cust) {
		this.custService.save(cust);
		return ApiResult.ok();
	}
	
	// 根据id查询客户信息: GetMapping, @PathVariable
	@GetMapping("/custs/{id}")
	public ApiResult findById(@PathVariable int id) {
		Cust cust = this.custService.findById(id);
		return ApiResult.ok(cust);
	}
	
	// 更新客户信息: PutMapping, @RequestBody
	@PutMapping("/custs")
	public ApiResult update(@RequestBody Cust cust) {
		this.custService.update(cust);
		return ApiResult.ok();
	}
	
	// 删除客户信息: DeleteMapping, @PathVariable
	@DeleteMapping("/custs/{id}")
	public ApiResult delete(@PathVariable int id) {
		this.custService.delete(id);
		return ApiResult.ok();
	}
	
}
