package com.crm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crm.mapper.DashboardMapper;
import com.crm.vo.ApiResult;

@CrossOrigin
@RestController
public class DashboardController {
	
	@Autowired
	private DashboardMapper mapper;
	
	@GetMapping("/sales")
	public ApiResult sales() {
		List<Map<String, Object>> sales = mapper.countSales();
		return ApiResult.ok(sales);
	}
	
}
