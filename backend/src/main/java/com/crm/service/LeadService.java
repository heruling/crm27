package com.crm.service;

import com.crm.entity.Lead;

/**
 * 线索模块的业务接口
 * 
 * @author root
 *
 */
public interface LeadService extends BaseService<Lead> {

	
	/**
	 * 把指定的线索转为客户
	 * 
	 * @param id 线索的唯一编号
	 */
	void changeToBeCust(int id);
}
