package com.crm.service;

import com.crm.vo.SearchResult;

/**
 * 业务接口的父接口
 * @author root
 *
 * @param <T>
 */
public interface BaseService<T> {
	/**
	 * 保存
	 * 
	 * @param lead 封装了新建界面上数据的对象
	 */
	void save(T o);

	/**
	 * 删除
	 * 
	 * @param id 待删除的对象的唯一编号
	 */
	void delete(int id);

	/**
	 * 精确查找
	 * 
	 * @param id 查找条件
	 * @return 如果有结果，返回查找到的对象；否则返回null
	 */
	T findById(int id);

	/**
	 * 更新
	 * 
	 * @param lead 封装了编辑界面上数据的对象
	 */
	void update(T o);

	/**
	 * 根据关键词搜索
	 * 
	 * @param q        关键词
	 * @param pageSize 每页几行
	 * @param pageNo   第几页
	 * @return SearchResult对象，该对象封装了数据对象的列表以及符合关键词的总数据条数
	 */
	SearchResult<T> search(String q, int pageSize, int pageNo);

}
