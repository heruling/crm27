package com.crm.service;

import java.util.List;

import com.crm.entity.Cust;

/**
 * 客户模块的业务接口	
 * 
 * @author root
 *
 */
public interface CustService extends BaseService<Cust> {

	List<Cust> searchByName(String query);


}
