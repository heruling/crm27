package com.crm.service;

import com.crm.entity.User;

/**
 * 用户这个模块的功能我都定义在这个接口中
 * 
 * @author root
 *
 */
public interface UserService {
	/**
	 * 用户身份认证
	 * 
	 * @param username 输入的用户名
	 * @param password 输入的密码
	 * @return 如果身份认证成功，返回该用户对象（对象中包含除密码外的信息）；否则，返回null
	 */
	User authenticate(String username, String password);

}
