package com.crm.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.entity.Cust;
import com.crm.entity.Lead;
import com.crm.mapper.CustMapper;
import com.crm.mapper.LeadMapper;
import com.crm.mapper.LeadTraceMapper;
import com.crm.service.LeadService;
import com.crm.util.AuthenticationUtil;
import com.crm.vo.SearchResult;

@Service
public class LeadServiceImpl implements LeadService {

	@Autowired
	private LeadMapper leadMapper;
	@Autowired
	private LeadTraceMapper leadTraceMapper;
	@Autowired
	private CustMapper custMapper;
	
	@Override
	public void save(Lead o) {
		// 由于用户并未在界面上填写创建时间和所有者id，需要在业务方法中进行补填。
		o.setCreateDate(new Date());
		o.setUserId(AuthenticationUtil.currentUserId());
		leadMapper.insert(o);
	}

	@Override
	public void delete(int id) {
		// 先删相关的子表记录lead_trace
		leadTraceMapper.deleteByLeadId(id);
		// 再删这个lead
		leadMapper.deleteById(id);
	}

	@Override
	public Lead findById(int id) {
		Lead lead = leadMapper.selectById(id);
		return lead;
	}

	@Override
	public void update(Lead o) {
		leadMapper.updateById(o);
	}

	@Override
	public SearchResult<Lead> search(String q, int pageSize, int pageNo) {
		// 查匹配的行数
		long total = leadMapper.selectTotal(q);
		// 查这一页的数据
		int skip = pageSize * pageNo - pageSize;
		int take = pageSize;
		List<Lead> data = leadMapper.selectPageList(q, skip, take);
		// 返回结果
		SearchResult<Lead> searchResult = new SearchResult<Lead>(data, total);
		return searchResult;
	}

	@Override
	@Transactional
	public void changeToBeCust(int id) {
		// 查出这个线索
		Lead lead = leadMapper.selectById(id);
		if (Objects.isNull(lead)) {
			throw new IllegalArgumentException("线索不存在: " + id);
		}
		// 插入到客户表
		Cust cust = new Cust();
		cust.setAddress(lead.getAddress());
		cust.setCreateDate(new Date());
		cust.setFrom(lead.getFrom());
		cust.setName(lead.getName());
		cust.setTel(lead.getTel());
		cust.setUserId(lead.getUserId());
		custMapper.insert(cust);
		// 删除这个线索的所有跟进
		final int leadId = id;
		leadTraceMapper.deleteByLeadId(leadId);
		// 删除这个线索
		leadMapper.deleteById(id);
		// 提交事务
	}

}
