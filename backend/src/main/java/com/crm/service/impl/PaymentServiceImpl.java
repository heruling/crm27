package com.crm.service.impl;

import org.springframework.stereotype.Service;

import com.crm.entity.Payment;
import com.crm.mapper.PaymentMapper;
import com.crm.service.PaymentService;

@Service
public class PaymentServiceImpl extends BaseServiceImpl<Payment, PaymentMapper> implements PaymentService {

}
