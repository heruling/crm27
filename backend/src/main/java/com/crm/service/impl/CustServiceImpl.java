package com.crm.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crm.entity.Cust;
import com.crm.mapper.CustMapper;
import com.crm.mapper.CustTraceMapper;
import com.crm.service.CustService;
import com.crm.util.AuthenticationUtil;

@Service
public class CustServiceImpl extends BaseServiceImpl<Cust, CustMapper> implements CustService {

	@Autowired
	private CustMapper custMapper;
	@Autowired
	private CustTraceMapper custTraceMapper;
	
	@Override
	public void save(Cust o) {
		// 补填信息
		o.setCreateDate(new Date());
		o.setUserId(AuthenticationUtil.currentUserId());
		// 调用父类的save
		super.save(o);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// 先删cust_trace
		custTraceMapper.deleteByCustId(id);
		// 再删cust
		custMapper.deleteById(id);
	}

	@Override
	public List<Cust> searchByName(String query) {
		return this.custMapper.selectByName(query);
	}

}
