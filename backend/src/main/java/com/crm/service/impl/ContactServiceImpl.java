package com.crm.service.impl;

import org.springframework.stereotype.Service;

import com.crm.entity.Contact;
import com.crm.mapper.ContactMapper;
import com.crm.service.ContactService;

@Service
public class ContactServiceImpl extends BaseServiceImpl<Contact, ContactMapper> implements ContactService {
	
}
