package com.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.crm.mapper.BaseMapper;
import com.crm.service.BaseService;
import com.crm.vo.SearchResult;

/**
 * 业务基类，封装各个实体通用的CRUD方法
 * @author Administrator
 *
 * @param <T>
 * @param <TMapper>
 */
public abstract class BaseServiceImpl<T, TMapper extends BaseMapper<T>> implements BaseService<T> {

	@Autowired
	private TMapper mapper;
	
	@Override
	public void save(T o) {
		mapper.insert(o);
	}

	@Override
	public void delete(int id) {
		mapper.deleteById(id);
	}

	@Override
	public T findById(int id) {
		return mapper.selectById(id);
	}

	@Override
	public void update(T o) {
		mapper.updateById(o);
	}

	@Override
	public SearchResult<T> search(String q, int pageSize, int pageNo) {
		long total = mapper.selectTotal(q);
		final int skip = pageSize * pageNo - pageSize;
		final int take = pageSize;
		List<T> list = mapper.selectPageList(q, skip, take);
		return new SearchResult<>(list, total);
	}

}
