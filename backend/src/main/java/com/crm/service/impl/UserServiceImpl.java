package com.crm.service.impl;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crm.entity.User;
import com.crm.mapper.UserMapper;
import com.crm.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	// 这是一个用户登录的身份认证程序
	public User authenticate(String username, String password) {
		// 1 根据username查到user
		User user = userMapper.selectByUsername(username);
		// 2 检查user是否为null
		if (Objects.isNull(user)) {
			return null;
		}
		// 3 检查密码是否正确
		if (!Objects.equals(password, user.getPassword())) {
			return null;
		}
		// 4 返回清洗掉password的user
		user.setPassword(null);
		return user;
	}

}
