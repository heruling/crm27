package com.crm.service.impl;

import org.springframework.stereotype.Service;

import com.crm.entity.Business;
import com.crm.mapper.BusinessMapper;
import com.crm.service.BusinessService;

@Service
public class BusinessServiceImpl extends BaseServiceImpl<Business, BusinessMapper> implements BusinessService {

}
