package com.crm.service.impl;

import org.springframework.stereotype.Service;

import com.crm.entity.Contract;
import com.crm.mapper.ContractMapper;
import com.crm.service.ContractService;

@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract, ContractMapper> implements ContractService {

}
