package com.crm.util;

public class AuthenticationUtil {
	
	// 把数据存储当前线程中，这样在程序的其它任何一个地方，都可以访问
	private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

	public static void setUserId(int userId) {
		threadLocal.set(userId);
	}
	
	/**
	 * 获取当前用户的id
	 * @return
	 */
	public static int currentUserId() {
		return threadLocal.get();
	}
	
}
