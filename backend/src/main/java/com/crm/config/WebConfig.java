package com.crm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.crm.interceptor.AuthTokenInterceptor;

@Configuration // 这是一个用于配置(config)的类，目的是为了告诉spring框架：我有哪些自定义配置
public class WebConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry
		.addInterceptor(new AuthTokenInterceptor()) // 注册拦截器
		.addPathPatterns("/**") // 拦截所有请求
		.excludePathPatterns("/login"); // 但是/login请求除外
	}
	
}
