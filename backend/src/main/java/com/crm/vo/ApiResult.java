package com.crm.vo;

/**
 * 统一各个API（Controller）的返回结果
 * 
 * @author root
 *
 */
public class ApiResult {

	// 业务结果码( NOT HTTP STATUS CODE )
	private int code;
	// 业务消息
	private String msg;
	// 业务数据
	private Object data;

	/**
	 * 默认的成功码，参数HTTP 200
	 */
	public final static int DEFAULT_OK_CODE = 20000;
	/**
	 * 默认的错误码，参照HTTP 500
	 */
	public final static int DEFAULT_ERROR_CODE = 50000;
	/**
	 * 身份认证失败时的错误码，参照HTTP 401
	 */
	public final static int NOT_AUTHENTICATED = 40001;
	/**
	 * 无权限访问资权时的错误码，参照HTTP 403
	 */
	public final static int NOT_AUTHORIZED = 40003;
	/**
	 * 找不到访问的资源时的错误码，参照HTTP 404 (表示URL地址找不到)
	 */
	public final static int NOT_FOUND_RESOURCE = 40004;
	
	public final static String DEFAULT_ERROR_MSG = "Error";

	public ApiResult(int code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static ApiResult build(int code, String msg, Object data) {
		return new ApiResult(code, msg, data);
	}

	public static ApiResult ok() {
		return ok(null);
	}

	public static ApiResult ok(Object data) {
		return build(DEFAULT_OK_CODE, "OK", data);
	}

	public static ApiResult error() {
		return error("Error");
	}

	public static ApiResult error(int code) {
		return error(code, DEFAULT_ERROR_MSG);
	}

	public static ApiResult error(String msg) {
		return error(DEFAULT_ERROR_CODE, msg);
	}

	public static ApiResult error(int code, String msg) {
		return build(code, msg, null);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
