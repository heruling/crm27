package com.crm.vo;

import java.util.List;

/**
 * 封装搜索结果的类
 * 
 * @author root
 *
 * @param <T> T可以是Lead、Cust、Contact、Business、Contract、Payment ...
 */
public class SearchResult<T> {

	// 搜索到的数据列表
	private List<T> list;
	// 符合条件的数据条数
	private long total;

	public SearchResult() {
	}

	public SearchResult(List<T> list, long total) {
		super();
		this.list = list;
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}
	
	public void setList(List<T> list) {
		this.list = list;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

}
