package com.crm.entity;

/**
 * 联系人类
 * 
 * @author root
 *
 */
public class Contact {
	private int id;
	private int custId;
	private String name;
	private String gender;
	private String phone;
	private String post;
	private Boolean isMaster;
	
	private Cust cust;
	
	public Cust getCust() {
		return cust;
	}
	
	public void setCust(Cust cust) {
		this.cust = cust;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public Boolean getIsMaster() {
		return isMaster;
	}

	public void setIsMaster(Boolean isMaster) {
		this.isMaster = isMaster;
	}

}
