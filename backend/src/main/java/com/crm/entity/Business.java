package com.crm.entity;

import java.math.BigDecimal;

/**
 * 商机类
 * 
 * @author root
 *
 */
public class Business {
	private int id;
	private int userId;
	private int custId;
	private String name;
	private BigDecimal money;
	private int stage;
	private Boolean endStatus;

	private User owner;
	private Cust cust;
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Cust getCust() {
		return cust;
	}

	public void setCust(Cust cust) {
		this.cust = cust;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public Boolean getEndStatus() {
		return endStatus;
	}

	public void setEndStatus(Boolean endStatus) {
		this.endStatus = endStatus;
	}

}
