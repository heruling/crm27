package com.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.crm.entity.CustTrace;

@Mapper
public interface CustTraceMapper {

	List<CustTrace> selectByCustId(int custId);
	
	void insert(CustTrace custTrace);

	void deleteByCustId(int id);
}
