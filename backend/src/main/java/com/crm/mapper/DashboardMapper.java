package com.crm.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface DashboardMapper {

	@Select("SELECT\r\n" + 
			"	DATE_FORMAT( create_date, '%Y-%m' ) as `year-month`,\r\n" + 
			"	SUM( money ) as `money` \r\n" + 
			"FROM\r\n" + 
			"	contract \r\n" + 
			"GROUP BY\r\n" + 
			"	DATE_FORMAT( create_date, '%Y-%m' )\r\n" + 
			"ORDER BY\r\n" + 
			"	DATE_FORMAT( create_date, '%Y-%m' ) ASC")
	List<Map<String, Object>> countSales();
	
}
