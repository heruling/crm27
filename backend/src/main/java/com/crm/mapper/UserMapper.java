package com.crm.mapper;

import java.io.Serializable;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.crm.entity.User;

@Mapper
public interface UserMapper {

	/**
	 * 根据用户名查找用户
	 * @param username
	 * @return
	 */
	@Select("SELECT * FROM `user` WHERE username = #{username}")
	User selectByUsername(String username);
	
	@Select("SELECT * FROM `user` WHERE id = #{id}")
	User selectById(Serializable id);
}
