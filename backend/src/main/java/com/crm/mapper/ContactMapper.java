package com.crm.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.crm.entity.Contact;

@Mapper
public interface ContactMapper extends BaseMapper<Contact> {

}
