package com.crm.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.crm.entity.Payment;

@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {

}
