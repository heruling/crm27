package com.crm.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.crm.entity.Lead;

@Mapper
public interface LeadMapper {

	List<Lead> selectPageList(@Param("q") String q, @Param("skip") int skip, @Param("take") int take);
	
	long selectTotal(String q);
	
	void insert(Lead lead);
	
	void deleteById(Serializable id);
	
	void updateById(Lead lead);
	
	Lead selectById(Serializable id);
}
