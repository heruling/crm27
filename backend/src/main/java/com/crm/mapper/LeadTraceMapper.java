package com.crm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeadTraceMapper {

	@Delete("DELETE FROM lead_trace WHERE lead_id = #{leadId}")
	void deleteByLeadId(int leadId);

}
