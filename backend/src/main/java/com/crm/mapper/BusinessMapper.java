package com.crm.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.crm.entity.Business;
import com.crm.entity.Lead;

@Mapper
public interface BusinessMapper extends BaseMapper<Business> {

	List<Business> selectPageList(@Param("q") String q, @Param("skip") int skip, @Param("take") int take);
	
	long selectTotal(String q);
	
	void insert(Business business);
	
	void deleteById(Serializable id);
	
	void updateById(Lead lead);
	
	Business selectById(Serializable id);
	
}
