package com.crm.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BaseMapper<T> {
	List<T> selectPageList(@Param("q") String q, @Param("skip") int skip, @Param("take") int take);

	long selectTotal(String q);

	void insert(T o);

	void deleteById(Serializable id);

	void updateById(T o);

	T selectById(Serializable id);
}
