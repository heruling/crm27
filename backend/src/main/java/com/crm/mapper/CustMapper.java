package com.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.crm.entity.Cust;

@Mapper
public interface CustMapper extends BaseMapper<Cust> {

	@Select("SELECT id, name FROM cust WHERE name LIKE CONCAT('%', #{query},'%')")
	List<Cust> selectByName(String query);
	
}
