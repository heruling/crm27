package com.crm.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.crm.entity.Contract;

@Mapper
public interface ContractMapper extends BaseMapper<Contract> {

}
