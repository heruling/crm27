package com.crm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.crm.util.AuthenticationUtil;

/**
 * handler拦截器，它用于拦截Controller中的那些handler methods
 * @author Administrator
 *
 */
public class AuthTokenInterceptor implements HandlerInterceptor {

	// preHandle指的是在请求映射到的目标handler method执行"之前(pre)"，先执行这里的拦截逻辑
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// 读取请求头中的Auth-Token
		String authToken = request.getHeader("Auth-Token");
		try {
			int userId = Integer.parseInt(authToken);
			AuthenticationUtil.setUserId(userId);
		} catch (Exception e) {
		}
		return true;
	}
	
}
